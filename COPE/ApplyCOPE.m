function [ score, response ] = ApplyCOPE( config, model, input )
% function [ score, response ] = ApplyCOPE( config, model, input )
% 
% This function computes the response of a COPE feature extractor on the
% time-frequency representation of a given audio signal.
% This is the slow implementation of the COPE feature extractor, suitable
% for computation of a single response but not efficient to be used for the
% computation of a large feature vector.
%
% ApplyCOPE takes as input:
%      config -> A structure with configuration parameters (see SystemConfig.m)
%      model -> A configured COPE model of a feature extractor (see
%               ConfigureCOPE.m)
%      input -> Time-frequency (e.g. Gammatonegram) representation of an
%               audio signal to extract the COPE feature from
%
% ApplyCOPE returns:
%      score     -> max-pooling of the COPE feature extractor response
%      response  -> response of the COPE feature extractor that slides on
%                   the input signal during time


    % Find local maxima in the time-frequency sound transform
    [ peaks, location ] = FindLocalMaxima( input, config.FILTERS.localmaxima.nhood, config.FILTERS.localmaxima.threshold );
    
    % Reconstruct model
    support = model.params.support;
    sigma0 = model.params.sigma0;
    alpha = model.params.alpha;
    
    ntuples = size(model.tuples, 1);
    xpad = round(support / 2);
    tupleimgs = zeros(size(input, 1), support + 2 * xpad, ntuples);
    
    % alpha == 0: no tolerance based on distance
    if alpha == 0
        sigma = sigma0 / 2;
        L = round( 3 * sigma);
        if mod(L, 2) == 0
            L = L + 1;
        end
        w = gaussian2d(L, sigma);
    end

    for n = 1:ntuples
        % Compute Gaussian weight function
        if alpha ~= 0
            rho = model.tuples(n, 4);
            sigma = ( sigma0 + alpha * rho ) / 2;
            L = round( 3 * sigma);
            if mod(L, 2) == 0
                L = L + 1;
            end
            w = gaussian2d(L, sigma);
        end
        
        % Sub-unit point
        y = round(size(input, 1) / 2) + model.tuples(n, 2);
        x = round(support / 2) + xpad + model.tuples(n, 1);
        
        % Position the Gaussian weight function in the right position
        % relative to the reference point in the filter support
        patchedimg = padarray(tupleimgs(:, :, n), [L 0]);
        patchedimg( y + L - round(L/2) + 1: y + L + size(w, 1) - round(L/2), x - round(L/2) + 1: x + size(w, 2) - round(L/2)) = w; %x + L - round(L/2): x + L + size(w, 2) - round(L/2) - 1
        patchedimg = patchedimg( L + 1: end - L, :); %L + 1: end - L);
        
        tupleimgs(:, :, n) = patchedimg;
    end
    
%     figure;
%     for n = 1:ntuples
%         subplot(7,3,n);
%         imagesc(tupleimgs(:, :, n)); colormap(gray);
%     end
    
    % Compute filter reponse
    responses = zeros(ntuples, size(input, 2));
    xlen = size(input, 2);
    response = ones(1, xlen);

    W = repmat(w, 1, ceil(xlen / L));
    shift = floor(L/2);
    ypad = shift;
    for n = 1:ntuples
        if config.FILTERS.subunit_function == 0
            I = padarray(peaks, [ypad 2 * xpad]);
        elseif config.FILTERS.subunit_function == 1
            I = padarray(1 - abs(peaks .* input - model.tuples(n, 3)) ./ model.tuples(n, 3), [ypad 2 * xpad]);
        end
        
        %resp = zeros(1, xlen);
        %weight_win = tupleimgs(:, :, n);
        %idx = find(weight_win > 0);
        
        Icrop = I(round(size(input, 1) / 2) + model.tuples(n, 2) - shift + ypad:round(size(input, 1) / 2) + model.tuples(n, 2) + shift + ypad, :);
        resps = zeros(ceil(xlen / L), L);

        for k = -shift:shift
            A = circshift(Icrop, [0 -k]);
            R = W .* A(:, 2 * xpad + 1 : 2 * xpad + size(W,2));
            resps(:, k + shift + 1) = max(reshape(R, L*L, []))'; %R(sh + 1, shift + 1:L:end)';
        end
        resp = reshape(resps', 1, []);
        resp = resp(1:size(input, 2));
        

%         j = 1;
%         while j <= xlen
%             Icrop = I(:, j:j + support + 2 * xpad - 1);
%             resp(j) = max(Icrop(idx) .* weight_win(idx));
% %             imresp = I(:, j:j + support + 2 * xpad - 1) .* tupleimgs(:, :, n);
% %             resp(j) = max(imresp(:));
%             j = j + 1;
%         end

        
        responses(n, :) = resp;
        response = response .* responses(n, :);
    end

    % Geometric mean
    response = response .^ (1 / ntuples);
    score = max(response);
end

