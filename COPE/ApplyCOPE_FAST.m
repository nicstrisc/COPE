function [ score, response ] = ApplyCOPE_FAST( model, inputSet )
% function [ score, response ] = ApplyCOPE_FAST( config, model, input )
% 
% This function computes the response of a COPE feature extractor on the
% time-frequency representation of a given audio signal.
%
% FAST implementation of COPE feature extraction, based on a pre-processed
% version of the input time-frequency (e.g. Gammatonegram) representation
% of the considered audio signal (see ApplyCOPEbank.m).
% Suitable for computation of COPE feature vectors.
%
% ApplyCOPE_FAST takes as input:
%      model    -> A configured COPE model of a feature extractor (see
%                  ConfigureCOPE.m)
%      inputSet -> Pre-processed time-frequency (e.g. Gammatonegram) representation 
%                  of an audio signal to extract the COPE feature from
%
% ApplyCOPE_FAST returns:
%      score     -> max-pooling of the COPE feature extractor response
%      response  -> response of the COPE feature extractor that slides on
%                   the input signal during time

    input = inputSet(model.params.sigma0);
    if iscell(input) == 1
        input = input{1};
    end
    ntuples = size(model.tuples, 1);
    % Do not compute responsed in case the number of tuples is less or
    % equala than 5. 
    if ntuples <= 5 
        score = 0;
        response = 0;
        return;
    end
    response = ones(1, size(input, 2));
    responses = zeros(ntuples, size(input, 2));
    c = round(size(input, 1) / 2);
    
    % 1-D response (during time)
    for n = 1:ntuples
        % Select the subunit
        v = input(c + model.tuples(n, 2), :);
        % Shifting
        v = circshift(v, [0 -model.tuples(n, 1)]);
        
        %% Score of the tuple (sub-part) - (Implement here the function phi(.,.) of the paper)
%         if config.FILTERS.subunit_function == 1
%             r = 1 - abs(v - model.tuples(n, 3)) ./ (v + model.tuples(n, 3));
%         elseif config.FILTERS.subunit_function == 0
%             r = v; % Take the weighted values of the energy peaks (model used only for position)
%         end
        % r = 1 - abs(v - model.tuples(n, 3)) ./ (v + model.tuples(n, 3));
        
        %% Tuple (sub-part) energy ratio - (Implement here the function phi(.,.) of the paper)
%         lambda = abs(model.tuples(n, 3)/model.params.peakvalue - v ./ input( model.point.y, :));
%         lambda(lambda > 1) = 1;
%         r = 1 - lambda;
        
        %% Sub-unit absolute energy value 
        r = v;
        
        responses(n, :) = r;
        response = response .* responses(n, :);
    end
    
    % Geometric mean
    response = response .^ (1 / ntuples); 
    % Max-pooling
    score = max(response);
end

